package com.kry.server;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ServerTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ServerTest(String testName){
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ServerTest.class);
    }

    /**
     * Rigourous Test :-)
     * 
     * @throws IOException
     * @throws ClientProtocolException
     */
    public void testEndpoints() throws ClientProtocolException, IOException {
        Server server = new Server();
        server.start();

        // Given
        HttpUriRequest request = new HttpGet("http://localhost:8080/service");

        // When
        CloseableHttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

        // Then
        assertEquals(httpResponse.getStatusLine().getStatusCode(), 200);
    }
}
