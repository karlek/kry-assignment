package com.kry.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ServiceTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     * @param testName name see test case
     */
    public ServiceTest(String testName){
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ServiceTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testLoadWithNonExistantFile() {
        Services services = new Services();
        try {
			services.load("/tmp/i-dont-exist");
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			// wat
        }
        assertEquals(services.size(), 0);
    }

    /**
     * Rigourous Test :-)
     *
     * @throws IOException
     */
    public void testSaveAndLoad() throws IOException, MalformedURLException {
        Services services = new Services();
        services.push(new Service(
            "test 1",
            new URL("https://wikipedia.org"),
            UUID.randomUUID())
        );
        services.save("/tmp/test-path");

        Services newServices = new Services();
        try {
			newServices.load("/tmp/test-path");
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			// wat
        }
        Gson g = new Gson();
        String servicesString = g.toJson(services);
        String newServicesString = g.toJson(newServices);
        assertEquals(servicesString, newServicesString);
    }

    public void testRemove() throws MalformedURLException {
        Services services = new Services();
        UUID id = UUID.randomUUID();
        services.push(new Service(
            "test 1",
            new URL("https://wikipedia.org"),
            id)
        );
        services.removeById(id);
        assertTrue(services.size() == 0);
    }

    public void testAlreadyExists() throws MalformedURLException {
        Services services = new Services();
        UUID id = UUID.randomUUID();
        services.push(new Service(
            "test 1",
            new URL("https://wikipedia.org"),
            id)
        );
        assertTrue(services.alreadyExists("test 1"));
    }
}
