package com.kry.error;

public class Error {
    String message;
    int code;

    public Error(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
