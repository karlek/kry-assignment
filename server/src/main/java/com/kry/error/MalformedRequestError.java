package com.kry.error;

public class MalformedRequestError extends Error {
    public MalformedRequestError() {
        super("The request was malformed and could not be parsed", 2);
    }
}