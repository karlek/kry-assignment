package com.kry.error;

public class ServiceAlreadyExistsError extends Error {
    public ServiceAlreadyExistsError() {
        super("A service with that name already exists", 4);
    }
}