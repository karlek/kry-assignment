package com.kry.error;

public class MalformedURLError extends Error {
    public MalformedURLError() {
        super("The supplied URL is malformed and could not be parsed", 1);
    }
}