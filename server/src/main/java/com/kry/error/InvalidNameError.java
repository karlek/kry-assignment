package com.kry.error;

public class InvalidNameError extends Error {
    public InvalidNameError() {
        super("The supplied name is invalid", 3);
    }
}