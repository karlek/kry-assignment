package com.kry.error;

public class InvalidIDError extends Error {
    public InvalidIDError() {
        super("The supplied id is invalid", 5);
    }
}