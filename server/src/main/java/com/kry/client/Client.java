package com.kry.client;

import java.time.Instant;

import com.kry.service.Service;
import com.kry.service.Status;

import org.apache.log4j.Logger;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;

public class Client {
    WebClient client;
    final static Logger logger = Logger.getLogger(Client.class);

    public Client() {
        Vertx vertx = Vertx.vertx();
        WebClientOptions options = new WebClientOptions()
        .setUserAgent("Kry-Healthchecker");
        client = WebClient.create(vertx, options);
    }

    public void Healthcheck(Service s) {
        boolean useSSL = s.url.getDefaultPort() == 443;
        int port = s.url.getPort() == -1 ? s.url.getDefaultPort() : s.url.getPort();

        client.get(port, s.url.getHost(), s.url.getPath())
            .ssl(useSSL).send(ar -> {
            Status status = new Status("FAIL");
            if (ar.succeeded()) {
                HttpResponse<Buffer> response = ar.result();
                if (response.statusCode() == 200) {
                    status = new Status("OK");
                }
            }
            if (status.isFail()) {
                logger.info("Healthcheck fail: " + s.url);
            }
            s.status = status;
            s.checked = Instant.now().toEpochMilli();
        });
    }
}