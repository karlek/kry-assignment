package com.kry.app;

import com.kry.server.Server;

public class App {
  public static void main(String[] args) {
    Server server = new Server();
    server.load();
    server.start();
  }
}