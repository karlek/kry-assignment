package com.kry.service;

public class Status {
    String message;
    public Status(String message) {
        this.message = message;
    }

    public boolean isFail() {
        return message.equalsIgnoreCase("FAIL");
    }
}