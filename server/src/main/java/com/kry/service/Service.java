package com.kry.service;

import java.net.URL;
import java.util.UUID;

public class Service {
    public String name;
    public URL url;
    public UUID id;
    public Status status = new Status("WAITING");
    public long checked;

    public Service(String name, URL url, UUID id) {
        this.name = name;
        this.url = url;
        this.id = id;
    }
}