package com.kry.service;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class Services extends LinkedList<Service> {
    private static final long serialVersionUID = 1L;
    static Gson g = new Gson();

    public boolean alreadyExists(String name) {
        for (Service s : this) {
            if (s.name.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean removeById(UUID id) {
        for (Service s : this) {
            if (s.id.equals(id)) {
                this.remove(s);
                return true;
            }
        }
        return false;
    }

    public void save(String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path, false));
        String jsonString = g.toJson(this);
        writer.write(jsonString);
        writer.close();
}

    public void load(String path)
    throws FileNotFoundException, JsonIOException, JsonSyntaxException {
        Type listType = new TypeToken<LinkedList<Service>>() {}.getType();
        JsonReader reader;
        reader = new JsonReader(new FileReader(path));
        LinkedList<Service> services = g.fromJson(reader, listType);
        this.clear();
        this.addAll(services);
    }
}