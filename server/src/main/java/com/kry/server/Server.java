package com.kry.server;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.kry.service.Service;
import com.kry.service.Services;
import com.kry.client.Client;
import com.kry.error.InvalidIDError;
import com.kry.error.MalformedRequestError;
import com.kry.error.MalformedURLError;
import com.kry.error.ServiceAlreadyExistsError;

import org.apache.log4j.Logger;

import io.vertx.core.json.JsonObject;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;


public class Server {
  // Protip, don't use tmp in real-life :+1:
  final String DATABASE_PATH = "/tmp/database";
  final Logger logger = Logger.getLogger(Server.class);
  final int port = 8080;
  final int PERIODICITY = 3000;

  Services services = new Services();
  Gson g = new Gson();

  private Handler<RoutingContext> listServices = ctx -> {
    // This handler will be called for every request
    HttpServerResponse response = ctx.response();
    response.putHeader("Content-type", "application/json");

    // enable chunked responses because we will be adding data as
    // we execute over other handlers. This is only required once and
    // only if several handlers do output.
    response.setChunked(true);

    String jsonString = g.toJson(services);
    response.end(jsonString);
  };

  private Handler<RoutingContext> corsHandler =
    CorsHandler.create("*")
    .allowedMethod(HttpMethod.GET)
    .allowedMethod(HttpMethod.POST)
    .allowedMethod(HttpMethod.DELETE)
    .allowedHeader("Access-Control-Allow-Method")
    .allowedHeader("Access-Control-Allow-Origin")
    .allowedHeader("Access-Control-Allow-Credentials")
    .allowedHeader("Content-Type");

  private Handler<RoutingContext> createService = ctx -> {
    // This handler will be called for every request
    HttpServerResponse response = ctx.response();
    response.putHeader("Content-type", "application/json");

    // enable chunked responses because we will be adding data as
    // we execute over other handlers. This is only required once and
    // only if several handlers do output.
    response.setChunked(true);

    JsonObject j = ctx.getBodyAsJson();
    if (j == null) {
      String jsonString = g.toJson(new MalformedRequestError());
      response.setStatusCode(400);
      response.end(jsonString);
      return;
    }
    String name = j.getString("name");
    String urlParam = j.getString("url");

    URL url;
    try {
      url = new URL(urlParam);
    } catch (MalformedURLException e) {
      logger.info(e);
      logger.info(urlParam);
      String jsonString = g.toJson(new MalformedURLError());
      response.setStatusCode(400);
      response.end(jsonString);
      return;
    }

    if (services.alreadyExists(name)) {
      String jsonString = g.toJson(new ServiceAlreadyExistsError());
      response.setStatusCode(400);
      response.end(jsonString);
      return;
    }

    Service s = new Service(
    name,
    url,
    UUID.randomUUID());

    services.add(s);
    response.setStatusCode(201);
    response.end();
  };

  private Handler<RoutingContext> deleteService = ctx -> {
    HttpServerResponse response = ctx.response();
    response.putHeader("Content-type", "application/json");

    String serviceIdParam = ctx.request().getParam("service_id");
    UUID id;
    try {
      id = UUID.fromString(serviceIdParam);
    } catch (IllegalArgumentException e) {
      logger.info(e);
      String jsonString = g.toJson(new MalformedRequestError());
      response.setStatusCode(400);
      response.end(jsonString);
      return;
    }

    Boolean removed = services.removeById(id);
    if (!removed) {
      String jsonString = g.toJson(new InvalidIDError());
      response.setStatusCode(400);
      response.end(jsonString);
      return;
    }

    response.end();
  };

  Client healthCheckClient = new Client();
  private Handler<Long> checkHealth = ctx -> {
    for (Service s : services) {
      healthCheckClient.Healthcheck(s);
    }

    // NOTE: This can probably be corrupted during a powerout, since we overwrite
    // the file directly. Investigate this before using this in prod.
    try {
      services.save(DATABASE_PATH);
    } catch (IOException e) {
      logger.error(e);
    }
    logger.info("Services has been saved.");
  };

  public void start() {
    Vertx vertx = Vertx.vertx();
    Router router = Router.router(vertx);

    // Check health of our services.
    vertx.setPeriodic(PERIODICITY, checkHealth);

    router.route().handler(corsHandler);
    router.route("/service").handler(BodyHandler.create());
    router.route(HttpMethod.GET, "/service").handler(listServices);
    router.route(HttpMethod.POST, "/service").handler(createService);
    router.route(HttpMethod.DELETE, "/service/:service_id").handler(deleteService);

    HttpServer server = vertx.createHttpServer();
    server.requestHandler(router::accept).listen(8080);

    logger.info(String.format("Server has been started. Serving requests on :%d", port));
  }

  public void load() {
    // Load our services.
    try {
      services.load(DATABASE_PATH);
    } catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
      logger.error(e);
    }
  }
}