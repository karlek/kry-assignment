import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css/normalize.css';
import './index.css';
import NewServiceForm from './NewServiceForm';
import ServiceTable from './ServiceTable'

class App extends React.Component {
    render() {
        return (
            <div className="App">
                <h1>Service center</h1>
                <ServiceTable />
                <NewServiceForm />
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));