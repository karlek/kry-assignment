import React from 'react';
import 'normalize.css/normalize.css';
import './index.css';
import axios from 'axios';

class NewServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {name: '', url: ''}
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(event) {
        console.log(this.state)
        this.refs.form.reset();
        event.preventDefault();

        if (this.state.name === "") {
            alert("Invalid name")
            return;
        }
        var url;
        try {
            url = new URL(this.state.url);
        } catch (error) {
            alert(error)
            return
        }
        console.log(url)
        axios.post(`http://localhost:8080/service`, this.state).then(res => {
            console.log(res)
        }).catch(err => {
            alert(err + err.response);
        })
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit} ref="form">
                <label>
                    Service name:
                    <input type="name" value={this.state.name} onChange={this.handleChange} name="name" />
                </label>
                <label>
                    Service URL:
                    <input type="url" value={this.state.url} onChange={this.handleChange} name="url" />
                </label>
                <input type="submit" value="Submit" />
            </form>
        )
    }
}

export default NewServiceForm;