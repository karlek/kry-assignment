import React from 'react';
import 'normalize.css/normalize.css';
import './index.css';
import axios from 'axios';

class DeleteServiceButton extends React.Component {
    constructor(props) {
        super(props);
        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        console.log("button was clicked");
        console.log("props", this.props.id)
        axios.delete(`http://localhost:8080/service/`+this.props.id)
        .then(res => {
            console.log(res)
        });
    }

    render() {
        return (
            <button
            onClick={this.handleClick}>{this.props.label}</button>
    )}
}

export default DeleteServiceButton;