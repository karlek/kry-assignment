import React from 'react';
import 'normalize.css/normalize.css';
import './index.css';
import DeleteServiceButton from './DeleteServiceButton';
import axios from 'axios';

class ServiceTable extends React.Component {
    constructor(props){
        super(props);
        this.state = { services: [] };
    }

    async updateTable() {
        axios.get(`http://localhost:8080/service`)
            .then(res => {
                var services = res.data
                this.setState({services: services});
        });
    }

    tick() {
        this.updateTable()
    }

    componentDidMount() {
        this.interval = setInterval(() => this.tick(), 1000);
        this.updateTable()
    }


    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        let list = this.state.services.map(service =>{
            return (
                <tr key={service.id}>
                    <td><DeleteServiceButton label="Delete service" id={service.id} /></td>
                    <td>{service.id}</td>
                    <td>{service.name}</td>
                    <td>{service.url}</td>
                    <td>{service.status.message}</td>
                    <td>{service.healthy}</td>
                </tr>
        )});

        return (
            <table>
                <thead></thead>
                <tbody>{list}</tbody>
            </table>
        )
    }
};

export default ServiceTable;